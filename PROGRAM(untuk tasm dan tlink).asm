; Tanda directive .model digunakan untuk memberitahukan 
; kepada assembler bentuk memory yang digunakan oleh program


; dalam model tiny code dan data menempati satu segment memory
; yang sama
.model tiny


; directive code menginstruksikan assembler untuk memulai
; penulisan kode segment
.code


; origin directive ini memberitahukan assembler 
; di mana kode program akan dimuat ke dalam memory
org 100h                                          
; org 100h memberitahukan assembler untuk memuat program mulai
; dari alamat 100h (256 byte) pada memory


start:
    ; menjalankan interupt 21h dengan nilai ah = 09h
    ; interupt ini berfungsi menampilkan suatu string ke dalam
    ; layar
    ; string yang akan dicetak diambil dari alamat variabel
    ; yang berisi string tersebut
    ; alamat variabel disimpan ke dalam register dx
    ; string tersebut harus diakhiri dengan tanda $
    ; sebagai penanda batas akhir dari string yang akan ditampilkan
	mov si, 0h
	mov ah, 09h
	lea dx, pesan_input
	int 21h

input:
    mov ah, 01h
    int 21h
    ;menjalankan interupt 21h dengan nilai ah = 01h
    ;interupt ini membaca karakter dari keyboard kemudian
    ;menyimpan karakter tersebut ke dalam register al
    cmp al, 13  ; membandingkan nilai karakter yang diinput
                ; dengan karakter enter 
    je proses
	cmp al, 8   ; membandingkan nilai karakter yang diinput
	            ; dengan karakter backspace/ tombol hapus
	je hapus    ; jika perbandingan sama, lompat ke label hapus 
    
    
    ; menyimpan setiap inputan karakter ke dalam variabel array
    ; kalimat
    mov kalimat[si], al
    inc si      ; menambahkan index array kalimat sebanyak 1
                ; si menyimpan banyaknya inputan karakter yang
                ; dimasukkan
    jmp input
    

hapus:

	cmp si, 0   ; membandingkan apakah banyaknya karakter yang 
	            ; dimasukkan sama dengan 0
	je input    ; jika benar, lompat ke label inputan
	            ; untuk membaca inputan karakter
	            
	dec si  ; mengurangi jumlah karakter yang diinput sebanyak 1
	
	; menghapus karakter yang berada pada index si dalam variabel
	; kalimat
	mov kalimat[si], 0
	
	; menjalankan interut 21h dengan nilai ah = 02h
	; interupt ini menuliskan/mencetak karakter spasi ke dalam 
	; layar 
	; karakter yang dicetak diambil dari register dl
	mov ah, 2h
	mov dl, 32
	int 21h
	
	; menjalankan interupt yang sama
	; dan menuliskan/mencetak karakter backspace ke dalam layar
	mov dl, 8
	int 21h
	
	jmp input
	
	
proses:
    
    mov bx, 0h ; bx = jumlah_kata
    
	cmp si, 0 ; membandingkan apakah banyaknya karakter yang 
	          ; dimasukkan sama dengan 0 
	je output ; jika benar, lompat ke label output
	
    mov cx, si  ; mengisi nilai cx = si
                ; cx menentukan banyaknya perulangan yang akan
                ; dijalankan pada perulangan selanjutnya
    
    mov di, 0h  ; register di menyimpan index/posisi karakter 
                ;yang sedang ditelusuri
                
    
    mov al, 0h  ; al dapat diibaratkan sebagai variabel masuk
                ; variabel masuk = 0h (false) secara default
    
    
    perulangan:            
    
      ; dl menyimpan setiap nilai karakter yang sedang ditelusuri
      ; berurutan dari karakter awal sampai karakter akhir
        mov dl, kalimat[di]
        inc di  ; menambahkan register di sebanyak 1 untuk 
                ; mengakses/ menelusuri karakter berikutnya
                ; dalam perulangan berikutnya 
        
        cmp dl, " " ; membandingkan nilai karakter yang sedang 
                    ;diakses sama dengan karakter spasi
        jne p1  ;   lompat ke p1 jika nilai karakter tidak sama 
                ;   dengan spasi
        je p2   ;   lompat ke p2 jika karakter sama dengan spasi
        
        p1:
            cmp al,0h ; membandingkan apakah nilai variabel masuk
                      ; sama dengan false
                       
            je p1_1
            jne p1_2
            
            
        p1_1:   ; hal ini menunjukkan karakter yang telah diakses 
                ; sebelumnya bukanlah bagian dari suatu kata
                ; karakter yang sedang diakses sekarang merupakan
                ; bagian dari kata baru
                ; jumlah_kata bertambah sebanyak 1
            
            
            ; variabel masuk = true
            ; hal ini menunjukkan saat ini karakter yang sedang
            ; ditelusuri berada dalam suatu kata        
            mov al, 1h        
            inc bx ; menambahkan jumlah kata sebanyak 1
            jmp continue
            
        
        ; hal ini menunjukkan karakter yang sedang diakses
        ; sekarang merupakan bagian dari kata sebelumnya
        ; tidak ada penambahan kata baru
        p1_2:
            jmp continue
            
            
        p2: ;   jika huruf sama dengan spasi
        
            ; variabel masuk = false
            ; hal ini menunjukkan saat ini karakter 
            ; tidak mengakses suatu kata   
            mov al, 0h
            jmp continue
            
        
        
    continue:
    loop perulangan
    

output:
    
    ; menjalankan interupt 21h dengan nilai ah = 09h
    ; interupt ini berfungsi menampilkan suatu string ke dalam
    ; layar
    ; string yang akan dicetak diambil dari alamat variabel
    ; yang berisi string tersebut
    ; alamat variabel disimpan ke dalam register dx
    ; string tersebut harus diakhiri dengan tanda $
    ; sebagai penanda batas akhir dari string yang akan ditampilkan
    mov ah, 09h
    lea dx, pesan_output
    int 21h
        
    mov ax, bx
    lea bx, string_kata
    call int_ke_string
    
    mov string_kata[si], "$"
    
    mov ah, 09h
    lea dx, string_kata
    int 21h
    
ret


;fungsi int_ke_string berfungsi untuk
;mengubah nilai angka menjadi string
;sehingga angka dapat ditampilkan ke layar
;sesuai dengan nilai ascii dari angka tersebut
;fungsi ini menerima inputan :
;AX = angka yang akan diubah ke dalam string
;BX = POINTER yang menunjukkan alamat variabel yang
;     menjadi tempat penyimpanan string angka tersebut
;fungsi ini mengembalikkan nilai :
;SI = banyaknya digit dari angka yang diinputkan

int_ke_string PROC
push dx
mov dx, 0h            
mov si, 0h

while:
    cmp ax, 0h
    je break
    
    push bx
    mov bx, 10
    div bx
    
    pop bx
    add dx, 30h
     
    mov bx[si], dx
    inc si
    mov dx, 0h
    
    jmp while
break:
         
    push si     
    mov di, si
    dec di
    mov si, 0h
    
balik:
    cmp si, di
    jge selesai
    
    mov dl, bx[si]
    mov dh, bx[di]
    
    mov bx[si], dh
    mov bx[di], dl
    
    inc si
    dec di
    

    
    jmp balik
    
    selesai:
    pop si
    pop dx
    

RET    
int_ke_string ENDP
	

.data
	string_kata db 6 dup (?)
	pesan_input db "Masukkan kalimat : ", 13, 10, "$"
	pesan_output db 13, 10, "Jumlah kata dalam kalimat : $"
	kalimat db ?
end start
